INTRODUCTION
------------

iLangL module is a plugin for Translation Management Tool module (TMGMT).
It uses iLangL (https://ilangl.com) service for automated translation of
the content.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tmgmt_ilangl

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/tmgmt_ilangl

REQUIREMENTS
------------

This module requires the following modules:

 * [Translation Management Tool](http://drupal.org/project/tmgmt)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * To start using this plugin you should customize the settings in
   Administration » Translation » Providers » iLangL (you can specify the name)
   and fill the iLangL plugin settings form with data obtained from the iLangL
   service manager, next just press CONNECT button to get your token for
   receiving data from iLangL service API.

MAINTAINERS
-----------

Current maintainers:
 * Bohdan Vasilyuk (Bohdan Vasyliuk) - https://www.drupal.org/u/bohdan-vasyliuk
 * Orest Yakymchuk (t-ores) - https://www.drupal.org/u/t-ores
